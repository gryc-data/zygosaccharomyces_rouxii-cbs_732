# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2021-05-18)

### Edited

* Build feature hierarchy.

## v1.0 (2021-05-18)

### Added

* The 27 annotated scaffolds of Zygosaccharomyces rouxii CBS 732 (source EBI, [GCA_000026365.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000026365.1)).
