## *Zygosaccharomyces rouxii* CBS 732

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJNA12502](https://www.ebi.ac.uk/ena/browser/view/PRJNA12502)
* **Assembly accession**: [GCA_000026365.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000026365.1)
* **Original submitter**: Genolevues Consortium

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM2636v1
* **Assembly length**: 9,764,635
* **#Chromosomes**: 7
* **Mitochondiral**: No
* **N50 (L50)**: 1,496,342 (3)

### Annotation overview

* **Original annotator**: Genolevures Consortium
* **CDS count**: 4991
* **Pseudogene count**: 66
* **tRNA count**: 272
* **rRNA count**: 9
* **Mobile element count**: 5
